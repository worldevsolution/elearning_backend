<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionExercicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_exercices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->string('explication');
            $table->integer('reponse');
            $table->integer('exercice_id')->unsigned()->index()->nullable();
            $table->foreign('exercice_id')->references('id')->on('exercices')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_exercices');
    }
}
