<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_user_id')->unsigned()->nullable();
            $table->integer('to_user_id');
            $table->string('title');
            $table->string('notif_type');
            $table->integer('notif_target_id');
            $table->text('content');
            $table->timestamps();

            $table->foreign('from_user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('set null');
            /*$table->foreign('to_user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('set null');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
