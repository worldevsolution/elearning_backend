<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    //
    use RestTrait;

    protected $fillable = ['name','filepath','question_exercice_id'];

    protected $dates = ['created_at','updated_at'];

    public function  __construct(array $attributes = [])
    {
        $this->files = ['filepath'];
        parent::__construct($attributes);
    }

    public function getFilePathAttribute($val)
    {
        if($val==null){
            return null;
        }
        return env('APP_URL').$val;
    }

    public function question(){
        return $this->belongsTo(QuestionExercice::class);
    }
}
