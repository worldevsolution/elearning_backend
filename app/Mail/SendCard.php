<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendCard extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Card $card)
    {
        //
        $this->card = $card;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('emails.sendcard');

        return $this->from('no-reply@mukete.com.ng')
            ->view('emails.sendcard')->with([
            'name' => $this->card->to,
            'from' => $this->card->from,
            'link' => $this->card->slug,
        ]);
    }
}
