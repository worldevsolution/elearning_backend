<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Exercice extends Model{

    //
    use RestTrait;
    protected $fillable = ['name', 'type', 'module_id', 'course_id'];
    protected $dates = ['created_at','updated_at'];

    public function module(){
        return $this->belongsTo('App\Module', 'module_id');
    }

    public function course(){
        return $this->belongsTo('App\Course', 'course_id');
    }

    public function questions(){
        return $this->hasMany(QuestionExercice::class);
    }
}
