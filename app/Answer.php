<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model{

    //
    use RestTrait;
    protected $fillable = ['note', 'user_id', 'question_exercice_id'];

    protected $dates = ['created_at','updated_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function exercice(){
        return $this->belongsTo(QuestionExercice::class);
    }
}
