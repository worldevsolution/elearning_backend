<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Annonce extends Model
{
    //
    use RestTrait;
    protected $fillable = ['title','content', 'category_id', 'expert_id'];

    protected $dates = ['created_at','updated_at'];


    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function expert(){
        return $this->belongsTo(Expert::class);
    }
}
