<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    //
    use RestTrait;
    protected $fillable = ['name', 'date', 'lieu', 'course_id'];
    protected $dates = ['created_at','updated_at'];


    public function sections(){
        return $this->hasMany(Section::class);
    }

    public function exercices(){
        return $this->hasMany('App\Exercice', 'module_id');
    }

    public function course(){
        return $this->belongsTo(Course::class);
    }
}
