<?php

namespace App\Http\Controllers;

use App\Mail;
use App\Helpers\RestHelper;
use App\Http\Requests\MailRequest;

class MailController extends Controller
{
    //
    /**
     * Start action, use to show all mails inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Mail::class);
    }


    /**
     * Show the form for creating a new mail.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created mail in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MailRequest $request)
    {
        return RestHelper::store(Mail::class,$request->all());
    }

    /**
     * Display the specified mail. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Mail::class,$id);
    }

    /**
     * Show the form for editing the specified mail.
     *
     * @param  \App\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function edit(Mail $mail)
    {
        //
    }

    /**
     * Update the specified mail in databse.
     *
     * @param MailRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MailRequest $request, $id)
    {
        return RestHelper::update(Mail::class,$request->all(),$id);
    }

    /**
     * Remove the specified mail from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Mail::class,$id);
    }
}
