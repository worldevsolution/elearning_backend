<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Helpers\RestHelper;
use App\Http\Requests\TagRequest;

class TagController extends Controller
{
    //
    /**
     * Start action, use to show all tags inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Tag::class);
    }


    /**
     * Show the form for creating a new tag.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created tag in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TagRequest $request)
    {
        return RestHelper::store(Tag::class,$request->all());
    }

    /**
     * Display the specified tag. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Tag::class,$id);
    }

    /**
     * Show the form for editing the specified tag.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        //
    }

    /**
     * Update the specified tag in databse.
     *
     * @param TagRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TagRequest $request, $id)
    {
        return RestHelper::update(Tag::class,$request->all(),$id);
    }

    /**
     * Remove the specified tag from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Tag::class,$id);
    }
}
