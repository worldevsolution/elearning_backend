<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Helpers\RestHelper;
use App\Http\Requests\AnswerRequest;

class AnswerController extends Controller
{
    //
    /**
     * Start action, use to show all answers inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Answer::class);
    }


    /**
     * Show the form for creating a new answer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created answer in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AnswerRequest $request)
    {
        return RestHelper::store(Answer::class,$request->all());
    }

    /**
     * Display the specified answer. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Answer::class,$id);
    }

    /**
     * Show the form for editing the specified answer.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Answer $answer)
    {
        //
    }

    /**
     * Update the specified answer in databse.
     *
     * @param AnswerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AnswerRequest $request, $id)
    {
        return RestHelper::update(Answer::class,$request->all(),$id);
    }

    /**
     * Remove the specified answer from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Answer::class,$id);
    }
}
