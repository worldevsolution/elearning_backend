<?php

namespace App\Http\Controllers;

use App\Media;
use App\Helpers\RestHelper;
use App\Http\Requests\MediaRequest;

class MediaController extends Controller
{
    //
    /**
     * Start action, use to show all medias inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Media::class);
    }

    /**
     * Show the form for creating a new media.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created media in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MediaRequest $request)
    {
        return RestHelper::store(Media::class,$request->all());
    }

    /**
     * Display the specified media. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Media::class,$id);
    }

    /**
     * Show the form for editing the specified media.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit(Media $media)
    {
        //
    }

    /**
     * Update the specified media in databse.
     *
     * @param MediaRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MediaRequest $request, $id)
    {
        return RestHelper::update(Media::class,$request->all(),$id);
    }

    /**
     * Remove the specified media from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Media::class,$id);
    }
}
