<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helpers\RestHelper;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    //
    /**
     * Start action, use to show all categories inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Category::class);
    }


    /**
     * Show the form for creating a new category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created category in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryRequest $request)
    {
        return RestHelper::store(Category::class,$request->all());
    }

    /**
     * Display the specified category. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Category::class,$id);
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified category in databse.
     *
     * @param CategoryRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CategoryRequest $request, $id)
    {
        return RestHelper::update(Category::class,$request->all(),$id);
    }

    /**
     * Remove the specified category from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Category::class,$id);
    }
}
