<?php

namespace App\Http\Controllers;

use App\Course;
use App\Helpers\RestHelper;
use App\Http\Requests\CourseRequest;

class CourseController extends Controller
{
    //
    /**
     * Start action, use to show all courses inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Course::class);
    }


    /**
     * Show the form for creating a new course.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created course in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CourseRequest $request)
    {
        return RestHelper::store(Course::class,$request->all());
    }

    /**
     * Display the specified course. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Course::class,$id);
    }

    /**
     * Show the form for editing the specified course.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified course in databse.
     *
     * @param CourseRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CourseRequest $request, $id)
    {
        return RestHelper::update(Course::class,$request->all(),$id);
    }

    /**
     * Remove the specified course from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Course::class,$id);
    }
}
