<?php

namespace App\Http\Controllers;

use App\Choice;
use App\Helpers\RestHelper;
use App\Http\Requests\ChoiceRequest;

class ChoiceController extends Controller
{
    //
    /**
     * Start action, use to show all choices inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Choice::class);
    }


    /**
     * Show the form for creating a new choice.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created choice in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ChoiceRequest $request)
    {
        return RestHelper::store(Choice::class,$request->all());
    }

    /**
     * Display the specified choice. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Choice::class,$id);
    }

    /**
     * Show the form for editing the specified choice.
     *
     * @param  \App\Choice  $choice
     * @return \Illuminate\Http\Response
     */
    public function edit(Choice $choice)
    {
        //
    }

    /**
     * Update the specified choice in databse.
     *
     * @param ChoiceRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ChoiceRequest $request, $id)
    {
        return RestHelper::update(Choice::class,$request->all(),$id);
    }

    /**
     * Remove the specified choice from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Choice::class,$id);
    }
}
