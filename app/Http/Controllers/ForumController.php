<?php

namespace App\Http\Controllers;

use App\Forum;
use App\Helpers\RestHelper;
use App\Http\Requests\ForumRequest;

class ForumController extends Controller
{
    //
    /**
     * Start action, use to show all forums inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Forum::class);
    }


    /**
     * Show the form for creating a new forum.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created forum in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ForumRequest $request)
    {
        return RestHelper::store(Forum::class,$request->all());
    }

    /**
     * Display the specified forum. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Forum::class,$id);
    }

    /**
     * Show the form for editing the specified forum.
     *
     * @param  \App\Forum  $forum
     * @return \Illuminate\Http\Response
     */
    public function edit(Forum $forum)
    {
        //
    }

    /**
     * Update the specified forum in databse.
     *
     * @param ForumRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ForumRequest $request, $id)
    {
        return RestHelper::update(Forum::class,$request->all(),$id);
    }

    /**
     * Remove the specified forum from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Forum::class,$id);
    }
}
