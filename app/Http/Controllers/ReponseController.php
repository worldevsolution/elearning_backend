<?php

namespace App\Http\Controllers;

use App\Reponse;
use App\Helpers\RestHelper;
use App\Http\Requests\ReponseRequest;

class ReponseController extends Controller
{
    //
    /**
     * Start action, use to show all reponses inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Reponse::class);
    }


    /**
     * Show the form for creating a new reponse.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created reponse in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ReponseRequest $request)
    {
        return RestHelper::store(Reponse::class,$request->all());
    }

    /**
     * Display the specified reponse. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Reponse::class,$id);
    }

    /**
     * Show the form for editing the specified reponse.
     *
     * @param  \App\Reponse  $reponse
     * @return \Illuminate\Http\Response
     */
    public function edit(Reponse $reponse)
    {
        //
    }

    /**
     * Update the specified reponse in databse.
     *
     * @param ReponseRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ReponseRequest $request, $id)
    {
        return RestHelper::update(Reponse::class,$request->all(),$id);
    }

    /**
     * Remove the specified reponse from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Reponse::class,$id);
    }
}
