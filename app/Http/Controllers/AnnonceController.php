<?php

namespace App\Http\Controllers;

use App\Annonce;
use App\Helpers\RestHelper;
use App\Http\Requests\AnnonceRequest;

class AnnonceController extends Controller
{
    //
    /**
     * Start action, use to show all annonces inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Annonce::class);
    }


    /**
     * Show the form for creating a new annonce.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created annonce in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AnnonceRequest $request)
    {
        return RestHelper::store(Annonce::class,$request->all());
    }

    /**
     * Display the specified annonce. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Annonce::class,$id);
    }

    /**
     * Show the form for editing the specified annonce.
     *
     * @param  \App\Annonce  $annonce
     * @return \Illuminate\Http\Response
     */
    public function edit(Annonce $annonce)
    {
        //
    }

    /**
     * Update the specified annonce in databse.
     *
     * @param AnnonceRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AnnonceRequest $request, $id)
    {
        return RestHelper::update(Annonce::class,$request->all(),$id);
    }

    /**
     * Remove the specified annonce from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Annonce::class,$id);
    }
}
