<?php

namespace App\Http\Controllers;

use App\Event;
use App\Helpers\RestHelper;
use App\Http\Requests\EventRequest;

class EventController extends Controller
{
    //
    /**
     * Start action, use to show all events inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Event::class);
    }


    /**
     * Show the form for creating a new event.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created event in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(EventRequest $request)
    {
        return RestHelper::store(Event::class,$request->all());
    }

    /**
     * Display the specified event. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Event::class,$id);
    }

    /**
     * Show the form for editing the specified event.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified event in databse.
     *
     * @param EventRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EventRequest $request, $id)
    {
        return RestHelper::update(Event::class,$request->all(),$id);
    }

    /**
     * Remove the specified event from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Event::class,$id);
    }
}
