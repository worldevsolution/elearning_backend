<?php

namespace App\Http\Controllers;

use App\Exercice;
use App\Helpers\RestHelper;
use App\Http\Requests\ExerciceRequest;

class ExerciceController extends Controller
{
    //
    /**
     * Start action, use to show all exercices inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Exercice::class);
    }


    /**
     * Show the form for creating a new exercice.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created exercice in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ExerciceRequest $request)
    {
        return RestHelper::store(Exercice::class,$request->all());
    }

    /**
     * Display the specified exercice. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Exercice::class,$id);
    }

    /**
     * Show the form for editing the specified exercice.
     *
     * @param  \App\Exercice  $exercice
     * @return \Illuminate\Http\Response
     */
    public function edit(Exercice $exercice)
    {
        //
    }

    /**
     * Update the specified exercice in databse.
     *
     * @param ExerciceRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ExerciceRequest $request, $id)
    {
        return RestHelper::update(Exercice::class,$request->all(),$id);
    }

    /**
     * Remove the specified exercice from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Exercice::class,$id);
    }
}
