<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Browser;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Mail;
use Setting;

class AuthController extends Controller
{

    /**
     * Get authenticated user details and auth credentials.
     *
     * @return JSON
     */
    public function getAuthenticatedUser(){
        if (Auth::check()) {
            $user = Auth::user();
            $token = JWTAuth::fromUser($user);

            return response()->success(compact('user', 'token'));
        } else {
            return response()->error('unauthorized', 401);
        }
    }

    /**
     * Authenticate user.
     *
     * @param Instance Request instance
     *
     * @return JSON user details and auth credentials
     */
    public function userLogin(Request $request){

        $credentials = $request->only('email', 'password');
        $email=$credentials["email"];
        $password=$credentials["password"];
        if($email==null)
            return response()->error('missing login', 403);
        if($password==null)
            return response()->error('missing password', 403);

        $validator = Validator::make(['email'=>$email], ['email'=>'email']);

        if($validator->fails()){
            $user= User::where("telephone","=",$email)->where("password","=",hash('sha256', $password))->first();
        }else{
            $user = User::where('email', '=', $email)->where("password","=",hash('sha256', ($password)))->first();
        }

        if(!isset($user))
            return response()->error(trans('auth.failed'), 401);

        /* if (isset($user->email_verified) && $user->email_verified == 0) {
             return response()->error('Email Unverified');
         }*/

        try {
            if (!$token = Auth::login($user)) {
                return response()->error(trans('auth.failed'), 401);
            }
        } catch (\JWTException $e) {
            return response()->error('Could not create token', 500);
        }

        $user = Auth::user();
        $token = JWTAuth::fromUser($user);

        return response()->success(compact('user', 'token'));
    }

    public function updateMe(Request $request){
        $user = Auth::user();
        $user->update($request->all());
        return response()->success(compact('user'));
    }
    public function refresToken(){
        $token = JWTAuth::getToken();

        if (! $token) {
            throw new BadRequestHttpException('Token not provided');
        }

        try {
            $token = JWTAuth::refresh($token);
        } catch (TokenInvalidException $e) {
            throw new AccessDeniedHttpException('The token is invalid');
        }

        //$user = Auth::user();
        return response()->success(compact('token'));
    }

    /**
     * Create new user.
     *
     * @param Instance Request instance
     *
     * @return JsonResponse user details and auth credentials
     */
    public function postRegister(Request $request)
    {

        $rule = [
            'email'      => 'required|email|unique:users,email',
            'password'   => 'required|min:5',
            'last_name'        => 'required',
            'telephone'  => 'required',
            'gender'  => 'required'
        ];
        if($request->telephone){
            $rule['telephone'] = 'min:9|max:255|unique:users,telephone';
        }


        $validator = Validator::make($request->all(), $rule);

        if($validator->fails()){
            return response()->error($validator->errors(), 422);
        }else{
            $user = new User();
            $user->telephone = $request->telephone;
            $user->email = trim(strtolower($request->email));
            $user->password = hash('sha256', $request->password);
            $user->last_name = $request->last_name;
            $user->first_name = $request->first_name;
            $user->gender = $request->gender;
            $user->address = $request->address;
            $user->skype = $request->skype;
            $user->url = $request->url;
            $user->save();

            $token = JWTAuth::fromUser($user);

            return response()->success(compact('user', 'token'));
        }
    }

    public function putMe(Request $request)
    {
        $user = Auth::user();
        $rule = [
            'email'      => 'required|email|unique:users,email,'.$user->id,
            'password'   => 'required|min:5',
            'last_name'        => 'required',
            'telephone'  => 'required',
            'gender'  => 'required'
        ];
        if($request->telephone!=null){
            $rule['telephone'] = 'min:9|max:255|unique:users,telephone,'.$user->id;
        }

        $validator = Validator::make($request->all(), $rule);

        if($validator->fails()){
            return response()->error($validator->errors(), 422);
        }

        $user->update($request->all());

        $user->save();
        return response()->success(compact('user'));
    }
}
