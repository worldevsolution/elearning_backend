<?php

namespace App\Http\Controllers;

use App\Role;
use App\Helpers\RestHelper;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{
    //
    /**
     * Start action, use to show all roles inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Role::class);
    }


    /**
     * Show the form for creating a new role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created role in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RoleRequest $request)
    {
        return RestHelper::store(Role::class,$request->all());
    }

    /**
     * Display the specified role. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Role::class,$id);
    }

    /**
     * Show the form for editing the specified role.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified role in databse.
     *
     * @param RoleRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RoleRequest $request, $id)
    {
        return RestHelper::update(Role::class,$request->all(),$id);
    }

    /**
     * Remove the specified role from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Role::class,$id);
    }
}
