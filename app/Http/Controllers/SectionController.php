<?php

namespace App\Http\Controllers;

use App\Section;
use App\Helpers\RestHelper;
use App\Http\Requests\SectionRequest;

class SectionController extends Controller
{
    //
    /**
     * Start action, use to show all sections inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Section::class);
    }


    /**
     * Show the form for creating a new section.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created section in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SectionRequest $request)
    {
        return RestHelper::store(Section::class,$request->all());
    }

    /**
     * Display the specified section. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Section::class,$id);
    }

    /**
     * Show the form for editing the specified section.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        //
    }

    /**
     * Update the specified section in databse.
     *
     * @param SectionRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SectionRequest $request, $id)
    {
        return RestHelper::update(Section::class,$request->all(),$id);
    }

    /**
     * Remove the specified section from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Section::class,$id);
    }
}
