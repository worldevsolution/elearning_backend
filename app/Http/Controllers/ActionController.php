<?php

namespace App\Http\Controllers;

use App\Action;
use App\Helpers\RestHelper;
use App\Http\Requests\ActionRequest;

class ActionController extends Controller
{
    //
    /**
     * Start action, use to show all actions inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Action::class);
    }

    /**
     * Show the form for creating a new action.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created action in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ActionRequest $request)
    {
        return RestHelper::store(Action::class,$request->all());
    }

    /**
     * Display the specified action. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Action::class,$id);
    }

    /**
     * Show the form for editing the specified action.
     *
     * @param  \App\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function edit(Action $action)
    {
        //
    }

    /**
     * Update the specified action in databse.
     *
     * @param ActionRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ActionRequest $request, $id)
    {
        return RestHelper::update(Action::class,$request->all(),$id);
    }

    /**
     * Remove the specified action from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Action::class,$id);
    }
}
