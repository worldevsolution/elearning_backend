<?php

namespace App\Http\Controllers;

use App\Expert;
use App\Helpers\RestHelper;
use App\Http\Requests\ExpertRequest;

class ExpertController extends Controller
{
    //
    /**
     * Start action, use to show all experts inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Expert::class);
    }


    /**
     * Show the form for creating a new expert.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created expert in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ExpertRequest $request)
    {
        return RestHelper::store(Expert::class,$request->all());
    }

    /**
     * Display the specified expert. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Expert::class,$id);
    }

    /**
     * Show the form for editing the specified expert.
     *
     * @param  \App\Expert  $expert
     * @return \Illuminate\Http\Response
     */
    public function edit(Expert $expert)
    {
        //
    }

    /**
     * Update the specified expert in databse.
     *
     * @param ExpertRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ExpertRequest $request, $id)
    {
        return RestHelper::update(Expert::class,$request->all(),$id);
    }

    /**
     * Remove the specified expert from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Expert::class,$id);
    }
}
