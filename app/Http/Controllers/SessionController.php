<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    //

    public function signIn(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return redirect()->intended('latest-updates');
        }
        return redirect()->back();
    }

    public function signUp(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'telephone' => 'required|numeric|digits:9',
            'password' => 'required|confirmed',
        ]);

        $user = new \App\User();
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->telephone = $request->input('telephone');
        $user->save();

        Auth::login($user);
        return redirect()->to('home');
    }
}
