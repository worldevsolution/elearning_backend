<?php

namespace App\Http\Controllers;

use App\Privilege;
use App\Helpers\RestHelper;
use App\Http\Requests\PrivilegeRequest;

class PrivilegeController extends Controller
{
    //
    /**
     * Start action, use to show all privileges inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Privilege::class);
    }

    /**
     * Show the form for creating a new privilege.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created privilege in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PrivilegeRequest $request)
    {
        return RestHelper::store(Privilege::class,$request->all());
    }

    /**
     * Display the specified privilege. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Privilege::class,$id);
    }

    /**
     * Show the form for editing the specified privilege.
     *
     * @param  \App\Privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function edit(Privilege $privilege)
    {
        //
    }

    /**
     * Update the specified privilege in databse.
     *
     * @param PrivilegeRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PrivilegeRequest $request, $id)
    {
        return RestHelper::update(Privilege::class,$request->all(),$id);
    }

    /**
     * Remove the specified privilege from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Privilege::class,$id);
    }
}
