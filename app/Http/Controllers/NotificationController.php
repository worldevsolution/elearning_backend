<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Helpers\RestHelper;
use App\Http\Requests\NotificationRequest;

class NotificationController extends Controller
{
    //
    /**
     * Start action, use to show all notifications inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Notification::class);
    }

    /**
     * Show the form for creating a new notification.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created notification in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(NotificationRequest $request)
    {
        return RestHelper::store(Notification::class,$request->all());
    }

    /**
     * Display the specified notification. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Notification::class,$id);
    }

    /**
     * Show the form for editing the specified notification.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        //
    }

    /**
     * Update the specified notification in databse.
     *
     * @param NotificationRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(NotificationRequest $request, $id)
    {
        return RestHelper::update(Notification::class,$request->all(),$id);
    }

    /**
     * Remove the specified notification from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Notification::class,$id);
    }
}
