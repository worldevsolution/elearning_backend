<?php

namespace App\Http\Controllers;

use App\Module;
use App\Helpers\RestHelper;
use App\Http\Requests\ModuleRequest;

class ModuleController extends Controller
{
    //
    /**
     * Start action, use to show all modules inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(Module::class);
    }

    /**
     * Show the form for creating a new module.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created module in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ModuleRequest $request)
    {
        return RestHelper::store(Module::class,$request->all());
    }

    /**
     * Display the specified module. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(Module::class,$id);
    }

    /**
     * Show the form for editing the specified module.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        //
    }

    /**
     * Update the specified module in databse.
     *
     * @param ModuleRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ModuleRequest $request, $id)
    {
        return RestHelper::update(Module::class,$request->all(),$id);
    }

    /**
     * Remove the specified module from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(Module::class,$id);
    }
}
