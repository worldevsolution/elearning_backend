<?php

namespace App\Http\Controllers;

use App\QuestionExercice;
use App\Helpers\RestHelper;
use App\Http\Requests\QuestionExerciceRequest;

class QuestionExerciceController extends Controller
{
    //
    /**
     * Start action, use to show all questionExercices inside the database
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return RestHelper::get(QuestionExercice::class);
    }

    /**
     * Show the form for creating a new questionExercice.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Action to be execute to store a newly created questionExercice in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionExerciceRequest $request)
    {
        return RestHelper::store(QuestionExercice::class,$request->all());
    }

    /**
     * Display the specified questionExercice. given the ID
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return RestHelper::show(QuestionExercice::class,$id);
    }

    /**
     * Show the form for editing the specified questionExercice.
     *
     * @param  \App\QuestionExercice  $questionExercice
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionExercice $questionExercice)
    {
        //
    }

    /**
     * Update the specified questionExercice in databse.
     *
     * @param QuestionExerciceRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(QuestionExerciceRequest $request, $id)
    {
        return RestHelper::update(QuestionExercice::class,$request->all(),$id);
    }

    /**
     * Remove the specified questionExercice from database given his id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return RestHelper::destroy(QuestionExercice::class,$id);
    }
}
