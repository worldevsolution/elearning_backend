<?php

namespace App\Http\Requests;

use App\Helpers\RuleHelper;
use Illuminate\Foundation\Http\FormRequest;

class MediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){

        $rules = [
            'name'=>'required|min:0|max:255',
            'type'=>'required',
            'filepath'=>'required|file',
            'section_id'=>'required|integer|exists:sections,id'
        ];
        return RuleHelper::get_rules($this->method(),$rules);
    }
}
