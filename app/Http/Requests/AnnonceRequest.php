<?php

namespace App\Http\Requests;

use App\Helpers\RuleHelper;
use Illuminate\Foundation\Http\FormRequest;

class AnnonceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'=>'required|min:0|max:255',
            'content'=>'required',
            'category_id'=>'required|integer|exists:categories,id',
            'expert_id'=>'integer|exists:experts,id',
        ];
        return RuleHelper::get_rules($this->method(),$rules);
    }
}
