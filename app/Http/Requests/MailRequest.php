<?php

namespace App\Http\Requests;

use App\Helpers\RuleHelper;
use Illuminate\Foundation\Http\FormRequest;

class MailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $rules = [
            'title'=>'required|min:0|max:255',
            'content'=>'required',
            'user_id'=>'required|integer|exists:users,id',
            'target_type'=>'required',
            'target_id'=>'required'
        ];
        return RuleHelper::get_rules($this->method(),$rules);
    }
}
