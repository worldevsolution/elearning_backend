<?php

namespace App\Http\Requests;

use App\Helpers\RuleHelper;
use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'=>'required|min:0|max:255',
            'start_date'=>'required|datetime',
            'end_date'=>'required|datetime',
            'course_id'=>'integer|exists:courses,id',
            'target_course_id'=>'integer|exists:courses,id',
            'user_id'=>'required|integer|exists:users,id'
        ];
        return RuleHelper::get_rules($this->method(),$rules);
    }
}
