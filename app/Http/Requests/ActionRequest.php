<?php

namespace App\Http\Requests;

use App\Helpers\RuleHelper;
use Illuminate\Foundation\Http\FormRequest;

class ActionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type'=>'required|min:0|max:255',
            'detail'=>'required|min:0|max:255',
            'user_id'=>'required|integer|exists:users,id',
            'course_id'=>'required|integer|exists:courses,id',
        ];
        return RuleHelper::get_rules($this->method(),$rules);
    }
}
