<?php

namespace App\Http\Requests;

use App\Helpers\RuleHelper;
use Illuminate\Foundation\Http\FormRequest;

class ExpertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $rules = [
            'user_id'=>'required|integer|exists:users,id',
            'category_id'=>'required|integer|exists:categories,id'
        ];
        return RuleHelper::get_rules($this->method(),$rules);
    }
}
