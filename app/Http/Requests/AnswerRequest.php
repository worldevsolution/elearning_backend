<?php

namespace App\Http\Requests;

use App\Helpers\RuleHelper;
use Illuminate\Foundation\Http\FormRequest;

class AnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'question'=>'required|min:0|max:255',
            'reponse'=>'required|integer',
            'user_id'=>'required|integer|exists:users,id',
            'question_exercice_id'=>'integer|exists:question_exercices,id',
        ];
        return RuleHelper::get_rules($this->method(),$rules);
    }
}
