<?php

namespace App\Http\Controllers;

use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['title'] = "Users";
        $data['users'] = \App\User::orderBy('created_at', 'desc')->paginate(20);
        return view('admin.users.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['title'] = "Create New User";
        return view('admin.users.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'email' => 'required',
            'name' => 'required',
            'type' => 'required',
        ]);

        // generate random password for user
        $password = str_random(12);

        $user = new \App\User();
        $user->email = $request->input('email');
        $user->name = $request->input('name');
        $user->password = Hash::make($password);
        $user->type = $request->input('type');
        $user->save();

        //  Send email to user with their details including thier password ...

        return redirect()->to('admin/users')->with('s', "User was created !");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data['title'] = "User Details";

        $data['user'] = \App\User::find($id);
        return view('admin.users.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['title'] = "Update User";
        $data['user'] = \App\User::find($id);
        return view('admin.users.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'type' => 'required',
            'status' => 'required',
        ]);

        $user = \App\User::find($id);
        $user->type = $request->input('type');
        $user->ban = $request->input('status');
        $user->save();
        return redirect()->to('admin/users')->with('s', "User was updated !");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = \App\User::find($id);
        $user->delete();
        return redirect()->to('admin/users')->with('s', "User and all content created by user deleted !");
    }
}
