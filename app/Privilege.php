<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    //
    use RestTrait;
    protected $fillable = ['name', 'key', 'role_id'];
    protected $dates = ['created_at','updated_at'];

    public function role(){
        return $this->belongsTo(Role::class);
    }
}
