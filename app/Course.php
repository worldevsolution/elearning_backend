<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    //
    use RestTrait;

    protected $fillable = ['name', 'picture', 'description', 'requirement', 'price', 'level', 'duration', 'expert_id'];
    protected $dates = ['created_at','updated_at'];

    public function  __construct(array $attributes = [])
    {
        $this->files = ['picture'];
        parent::__construct($attributes);
    }

    public function getPictureAttribute($val)
    {
        if($val==null){
            return nuul;
        }
        return env('APP_URL').$val;
    }

    public function expert(){
        return $this->belongsTo(Expert::class);
    }

    public function tags(){
        return $this->hasMany(Tag::class);
    }

    public function modules(){
        return $this->hasMany(Module::class);
    }

    public function exercices(){
        return $this->hasMany('App\Exercice', 'course_id');
    }

    public function events(){
        return $this->hasMany('App\Event', 'course_id');
    }

    public function subscribers(){
        return $this->hasMany(Action::class)->where('actions.type', '=', 'Subscribe');
    }

    public function ratings(){
        return $this->hasMany(Action::class)->where('actions.type', '=', 'Rate');
    }

    public function actions(){
        return $this->hasMany(Action::class);
    }
}
