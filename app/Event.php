<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    use RestTrait;

    protected $fillable = ['title', 'start_date', 'end_date', 'content', 'course_id', 'target_course_id', 'user_id'];
    protected $dates = ['created_at','updated_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function course(){
        return $this->belongsTo('App\Course', 'course_id');
    }

    public function target_course(){
        return $this->belongsTo('App\Course', 'target_course_id');
    }
}
