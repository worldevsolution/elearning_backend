<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    use RestTrait;
    protected $fillable = ['name', 'picture'];
    protected $dates = ['created_at','updated_at'];

    public function  __construct(array $attributes = [])
    {
        $this->files = ['picture'];
        parent::__construct($attributes);
    }

    public function getPictureAttribute($val)
    {
        if($val==null){
            return null;
        }
        return env('APP_URL').$val;
    }

    public function experts()
    {
        return $this->hasMany(Expert::class);
    }
    public function annonces()
    {
        return $this->hasMany(Annonce::class);
    }
}
