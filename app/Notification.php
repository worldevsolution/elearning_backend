<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model{
    //
    use RestTrait;
    protected $fillable = ['title', 'content', 'notif_type', 'notif_target_id', 'to_user_id', 'from_user_id'];
    protected $dates = ['created_at','updated_at'];

    public function from_user(){
        return $this->belongsTo('App\User', 'from_user_id');
    }

    public function to_user(){
        return $this->belongsTo('App\User', 'to_user_id');
    }
}
