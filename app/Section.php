<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    //
    use RestTrait;
    protected $fillable = ['name', 'content', 'module_id'];
    protected $dates = ['created_at','updated_at'];

    public function medias(){
        return $this->hasMany(Media::class);
    }
    public function module(){
        return $this->belongsTo(Module::class);
    }
}
