<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    //
    use RestTrait;
    protected $fillable = ['title', 'content', 'user_id', 'category_id'];
    protected $dates = ['created_at','updated_at'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function reponses(){
        return $this->hasMany(Reponse::class);
    }
}
