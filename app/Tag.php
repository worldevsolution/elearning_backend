<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    use RestTrait;
    protected $fillable = ['name', 'course_id'];
    protected $dates = ['created_at','updated_at'];

    public function course(){
        return $this->belongsTo(Course::class);
    }
}
