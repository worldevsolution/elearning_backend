<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    use RestTrait;
    protected $fillable = ['name', 'type', 'filepath', 'section_id'];
    protected $dates = ['created_at','updated_at'];

    public function  __construct(array $attributes = [])
    {
        $this->files = ['filepath'];
        parent::__construct($attributes);
    }

    public function getFilepathAttribute($val)
    {
        if($val==null){
            return null;
        }
        return env('APP_URL').$val;
    }

    public function section(){
        return $this->belongsTo(Section::class);
    }
}
