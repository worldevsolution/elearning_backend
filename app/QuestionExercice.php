<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class QuestionExercice extends Model
{
    //
    use RestTrait;
    protected $fillable = ['question', 'explication', 'reponse', 'exercice_id'];
    protected $dates = ['created_at','updated_at'];


    public function exercice(){
        return $this->belongsTo(Exercice::class);
    }

    public function choices(){
        return $this->hasMany(Choice::class);
    }

    public function answers(){
        return $this->hasMany(Answer::class);
    }
}
