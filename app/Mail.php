<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Mail extends Model{
    //
    use RestTrait;
    protected $fillable = ['title', 'content', 'user_id', 'target_type', 'target_id'];
    protected $dates = ['created_at','updated_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
