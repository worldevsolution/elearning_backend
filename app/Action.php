<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    //
    use RestTrait;
    protected $fillable = ['type','detail', 'field', 'user_id', 'course_id'];

    protected $dates = ['created_at','updated_at'];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function course(){
        return $this->belongsTo(Course::class);
    }
}
