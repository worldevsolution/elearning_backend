<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Expert extends Model
{
    //
    use RestTrait;
    protected $fillable = ['id', 'bibliography', 'user_id', 'category_id'];
    protected $dates = ['created_at','updated_at'];

    public function courses(){
        return $this->hasMany(Course::class);
    }

    public function annonces()
    {
        return $this->hasMany(Annonce::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function ratings(){
        return $this->hasManyThrough('App\Action', 'App\Course')->where('actions.type', '=', 'Rate');
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
