<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use RestTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','picture','telephone','url','skype','address', 'email'];
    protected $dates = ['created_at','updated_at'];

    public function  __construct(array $attributes = [])
    {
        $this->files = ['picture'];
        parent::__construct($attributes);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getPictureAttribute($val){
        if($val==null){
            $val='img/user/default.png';
        }
        return env('APP_URL').$val;
    }

    public function roles()
    {
        return $this->hasMany(Role::class);
    }

    public function experts(){
        return $this->hasMany(Expert::class);
    }

    public function forums(){
        return $this->hasMany(Forum::class);
    }

    public function reponses(){
        return $this->hasMany(Reponse::class);
    }

    public function events(){
        return $this->hasMany(Event::class);
    }

    public function mails(){
        return $this->hasMany(Mail::class);
    }

    public function answers(){
        return $this->hasMany(Answer::class);
    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
