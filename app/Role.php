<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    use RestTrait;
    protected $fillable = ['name', 'user_id'];
    protected $dates = ['created_at','updated_at'];


    public function privileges(){
        return $this->hasMany(Privilege::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
