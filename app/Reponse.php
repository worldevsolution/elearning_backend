<?php

namespace App;

use App\Traits\RestTrait;
use Illuminate\Database\Eloquent\Model;

class Reponse extends Model{

    //
    use RestTrait;
    protected $fillable = ['title', 'content', 'forum_id', 'user_id'];
    protected $dates = ['created_at','updated_at'];

    public function forum(){
        return $this->belongsTo(Forum::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
