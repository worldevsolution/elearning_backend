<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'code_sent' => 'Code sent.',
    'code_invalid' => 'Code is invalid.',
    'verification_done' => 'Verification done.',
    'device_banned' => 'This device is banned by the account user'

];
