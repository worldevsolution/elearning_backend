<?php

Route::get('/sorry', function () {

    return view('sorry');
});

Route::group(['middleware' => 'auth'],
    function () {
        Route::get('/', function () {
            $data['title'] = trans('site.title_home');
            return view('welcome')->with($data);
        });

    });

Route::post('sign-in', 'SessionController@signIn');
Route::post('sign-up', 'SessionController@signUp');

Route::get('/', function () {
    $data['title'] = trans('site.title_home');
    return view('welcome')->with($data);
});
