<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['namespace'=>'App\Http\Controllers'],function (Router $api){

        $api->group(['prefix' => 'auth'], function(Router $api) {
            $api->post('login', 'Auth\AuthController@userLogin');
            $api->post('signup', 'Auth\AuthController@postRegister');
            $api->get('me', 'Auth\AuthController@getAuthenticatedUser');
            $api->get('refresToken', 'Auth\AuthController@refresToken');
        });

        $api->group(['middleware' => 'auth'], function () {

        });

        $api->resource("actions", 'ActionController');
        $api->resource("annonces", 'AnnonceController');
        $api->resource("answers", 'AnswerController');
        $api->resource("categories", 'CategoryController');
        $api->resource("choices", 'ChoiceController');
        $api->resource("courses", 'CourseController');
        $api->resource("events", 'EventController');
        $api->resource("exercices", 'ExerciceController');
        $api->resource("experts", 'ExpertController');
        $api->resource("forums", 'ForumController');
        $api->resource("mails", 'MailController');
        $api->resource("medias", 'MediaController');
        $api->resource("modules", 'ModuleController');
        $api->resource("notifications", 'NotificationsController');
        $api->resource("privileges", 'PrivilegeController');
        $api->resource("questions", 'QuestionExerciceController');
        $api->resource("reponses", 'ReponseController');
        $api->resource("roles", 'RoleController');
        $api->resource("sections", 'SectionController');
        $api->resource("tags", 'TagController');
        $api->resource("users", 'UserController');

    });
});